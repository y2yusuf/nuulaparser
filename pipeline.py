import pandas as pd
import xml.etree.cElementTree as ET
import os
import sys
import xlsxwriter

# Load xml file names and check for missing or incorrect folders
def load_files_names(args):
    try:
        folder = args[1]
        file_list = os.listdir(folder)
    except Exception as e:
        print("FOLDER ERROR: {\"Message\": \"load_files_names: " + str(e) + "\"}\n")
        return []
    else:
        return [os.path.join(folder, files) for files in file_list if files.endswith('.xml')]

# Load xlsx files and check for missing or incorrect folders
def open_xlsx(folder_path):
    try:
        writer = pd.ExcelWriter(folder_path, engine='xlsxwriter')
    except Exception as e:
        print("FILE ERROR: {\"file\":" + folder_path + "\", \"Message\": \"open_xlsx: " + str(e) + "\"}\n")
        sys.exit()
    else:
        return writer


# Define Dataset Class
class Dataset():

    def __init__(self, path):
        self.entries = []
        self.path = path.split("/")
        self.df = pd.DataFrame()
        self.name = self.path[-1]

# Parse the xml file and return list of dictionary objects
    def extract_path(self, xml_path):
        if xml_path is not None:
            return [node.attrib for i, node in enumerate(xml_path)]
        return []

# Extract data from XML file and check if there are parsing errors
    def extract(self, file_path):
        try:
            root = ET.parse(file_path).getroot()
        except Exception as e:
            print("FILE ERROR: {\"file\":" + file_path + "\", \"Message\": \"Dataset.extract: " + str(e) + "\"}\n")
        else:
            for i in list(filter(None,self.path)):
                root = root.find(i)
            self.entries = self.entries + self.extract_path(root)
        return self

# Apply standard transformation to the dataset:
    # 1) define lower case column names
    # 2) fill NaN entries
    # 3) change column formats
    def transform(self):
        self.df = pd.DataFrame(self.entries)
        self.df.columns = self.df.columns.str.lower()
        self.df = self.df.fillna('')
        for i in self.df.columns:
            self.df[i] = self.df[i].astype(str)
        if 'timestamp' in self.df.columns:
            self.df['timestamp'] = pd.to_datetime(self.df['timestamp']).dt.tz_localize(None)
        return self

    def load(self, writer):
        self.df.to_excel(writer, sheet_name=self.name)
        return self

# Classes defined to overload transformation method in case transformation vary for each subclass of Dataset
class Errors(Dataset):
    def transform(self):
        super().transform()
        return self


class Messages(Dataset):
    def transform(self):
        super().transform()
        return self


class Rules(Dataset):
    def transform(self):
        super().transform()
        return self


if __name__ == "__main__":

# Define location of excel here
    load_path = 'logs.xlsx'

# Load names of all the xml files from given location
    xml_files = load_files_names(sys.argv)
    if len(xml_files) == 0:
        print("SUCCESS: {\"Message\": \"main: No files to load\"}\n")
        sys.exit()

# Open file path
    writer = open_xlsx(load_path)

# Define Dataset - NOTE transformations are inherited from parent Dataset class and can be overwritten
    package = [Errors("Nuula/Errors"),
               Messages("DataExtract900jer/Messages"),
               Rules("DataExtract900jer/Rules")]

# Perform ETL (Extract, Transform and Load) for each dataset for each file. Extraction done for all  files fist
# and bulk transform and load.

    for files in xml_files:
        for datasets in package:
            datasets.extract(files)

# Write to Excel File
    for datasets in package:
        datasets.transform()
        datasets.load(writer)

    writer.save()
    print("SUCCESS: {\"file\":" + load_path + "\", \"Message\": \" Excel File Loaded \"\n")
