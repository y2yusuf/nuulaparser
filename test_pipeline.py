#!/usr/bin/python
import unittest
import pipeline
import mock
from io import StringIO
import pandas as pd
import xml
from pandas.testing import assert_frame_equal

def xml_list(arg):
    if arg == 1:
        return True
    else:
        return False

class MockParser:
    def __init__(self, parse):
        self.parse = xml.etree.ElementTree.fromstring(parse)

    def getroot(self):
        return self.parse


class TestPipeline(unittest.TestCase):

    def setUp(self):
        self.sample_correct_xml = "<Data><text category = \"message\" purpose =\"Greet\" final = \"Hello Geeks\"/><text category = \"messa\" purpose =\"GoodBye\" final = \"Bye Geeks\"/></Data>"
        self.sample_incorrect_xml = "<Data><text category = \"message\" purpose =\"Greet\" final = \"Hello Geeks\"/><text category = "

#Test loading of XML File Paths and Names - Correct Path

    @mock.patch('os.listdir', return_value = ['a.xml','b.xml'])
    def test_load_files_names____correct_path(self,listdir):
        actual = pipeline.load_files_names(["test.py","data\\"])
        expected = ['data\\a.xml','data\\b.xml']
        self.assertEqual(expected, actual)

#Test loading of XML File Paths and Names - Files Not Found

    @mock.patch('sys.stdout', new_callable=StringIO)
    @mock.patch('os.listdir', side_effect=FileNotFoundError("Test"))
    def test_load_files_names____incorrect_path(self, listdir,  stdout):
        pipeline.load_files_names(["test.py", "data\\"])
        self.assertEqual(stdout.getvalue(), "FOLDER ERROR: {\"Message\": \"load_files_names: Test\"}\n\n")

#Test loading of XML File Paths and Names - Files with different extensions

    @mock.patch('os.listdir', return_value = ['a.xml','b.xml','c.txt','dfsdf.fdf'])
    def test_load_files_names____correct_path_with_extra_files(self,listdir):
        actual = pipeline.load_files_names(["test.py","data\\"])
        expected = ['data\\a.xml','data\\b.xml']
        self.assertEqual(expected, actual)

# Test Loading of Excel File for Output - Correct Path

    @mock.patch('pandas.ExcelWriter', return_value = True)
    def test_open_xlsx____correct_path(self,listdir):
        actual = pipeline.open_xlsx('open.xlsx')
        expected = True
        self.assertEqual(expected, actual)

# Test Loading of Excel File for Output - Permission Issues

    @mock.patch('sys.stdout', new_callable=StringIO)
    @mock.patch('pandas.ExcelWriter', side_effect=PermissionError("Test"))
    def test_open_xlsx____permission_error(self, ExcelWriter,  stdout):
        self.assertRaises(SystemExit, pipeline.open_xlsx, 'open.xlsx')

# Test Loading of Excel File for Output - Incorrect Path

    @mock.patch('pandas.ExcelWriter', return_value = False)
    def test_open_xlsx____incorrect_path(self,listdir):
        actual = pipeline.open_xlsx('open.xlsx')
        expected = False
        self.assertEqual(expected, actual)

# Extraction of data into list of dictionaries - Correct XML Format

    @mock.patch('xml.etree.cElementTree.parse', side_effect=MockParser)
    def test_Dataset_extract_extract_path____correct_xml(self, parse):
        test_dataset = pipeline.Dataset("")
        expected = [{'category': 'message', 'purpose': 'Greet', 'final': 'Hello Geeks'}, {'category': 'messa', 'purpose': 'GoodBye', 'final': 'Bye Geeks'}]
        test_dataset.extract(self.sample_correct_xml)
        self.assertEqual(expected, test_dataset.entries)

# Extraction of data into list of dictionaries - Incorrect XML Format

    @mock.patch('sys.stdout', new_callable=StringIO)
    @mock.patch('xml.etree.cElementTree.parse', side_effect=ValueError("Test"))
    def test_Dataset_extract_extract_path____incorrect_xml(self, parse, stdout):
        test_dataset = pipeline.Dataset("")
        expected = [{'category': 'message', 'purpose': 'Greet', 'final': 'Hello Geeks'}, {'category': 'messa', 'purpose': 'GoodBye', 'final': 'Bye Geeks'}]
        test_dataset.extract(self.sample_correct_xml)
        self.assertEqual(stdout.getvalue(), "FILE ERROR: {\"file\":" + self.sample_correct_xml + "\", \"Message\": \"Dataset.extract: Test\"}\n\n")

# Transformation DataFrame comparison

    def test_Dataset_transform_____dataframe_compare(self):
        test_dataset = pipeline.Dataset("")
        test_dataset.entries = [{'category': 'message', 'purpose': 'Greet', 'final': None, 'timestamp':'2017-12-15 19:02:35-0800'},
                                {'category': 3, 'purpose': 'GoodBye', 'final': 'Bye Geeks','timestamp':'2013-11-11 19:02:35-0800'},
                                {'category': 'messa', 'purpose': 'GoodBye'}]
        sample_df = pd.DataFrame(
                    {
                     'category':    ['message', '3', 'messa']
                     ,'purpose':    ['Greet', 'GoodBye', 'GoodBye']
                     ,'final':      ['', 'Bye Geeks', '']
                     ,'timestamp':  ['2017-12-15 19:02:35-0800','2013-11-11 19:02:35-0800','']
                     }
        )
        sample_df['timestamp'] = pd.to_datetime(sample_df['timestamp']).dt.tz_localize(None)
        test_dataset.transform()
        assert_frame_equal(test_dataset.df,sample_df)




if __name__ == "__main__":
    unittest.main()