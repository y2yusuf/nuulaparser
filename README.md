
**NOTES:**

 
1. This is a parser script called "pipeline". It accepts only a single argument which is the folder path to the data folder holding the xml files
 
2. It also comes with a test script designed to perform path coverage of the code in the "pipeline" script 

3. The requirements.txt provide information on what libraries are required to run this.

4. Simply call each script from a shell terminal using "python pipeline.py #folderpath#" or "python test_pipeline.py"

5. The output "log.xlsx" file will contain all three sheets


